const ExecuteService = require('./execute');
const Options = require('./options');
const RabbitMQ = require('amqp-node');

function init () {
    RabbitMQ.engineOpen(
        Options.rabbitmqName,
        {
            url: Options.rabbitmqUrl
        }
    );
    process.nextTick(services);
}

function serviceExecute () {
    if (Options.services && Options.services.execute)
        ExecuteService(Options.services.execute);
}

function services () {
    process.nextTick(serviceExecute);
}

init();
