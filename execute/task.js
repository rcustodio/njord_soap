const basicAuth = require('../auth').basicAuth;
const moment = require('moment');
const Options = require('../options');
const Publish = require('amqp-node/publish');
const routingKey = require('amqp-node/static').routingKey;
const Soap = require('soap');

function auth (msg) {
    if (msg.body.username && msg.body.password)
        msg.wsdl_headers.Authorization = basicAuth(msg.body.username,msg.body.password);
    return process.nextTick(genClient,msg);
}

function broadcast (channel, headers, body) {
    Publish.push(
        channel,
        Options.tornpackRabbitmqExchange.default,
        '',
        body || '',
        {
            headers: headers
        }
    );
}

function genClient (msg) {
    function onCreate (err, client) {
        if (err) {
            if (Options.fallbackToSuds)
                return process.nextTick(tryWithSuds,msg);
            process.nextTick(
                broadcast,
                msg.channel,
                {
                    code: Options.codes.execute.clientError,
                    etag: msg.properties.headers.etag
                },
                new Buffer(JSON.stringify({responseMsg: err.message || err})).toString('base64')
            );
            return msg.ack(true);
        }
        process.nextTick(getMethod,client,msg);
    }
    return Soap.createClient(
        msg.body.url,
        {
            wsdl_headers: msg.wsdl_headers
        },
        onCreate
    );
}

function getMethod (client, msg) {
    var method = client[msg.body.method];
    if (typeof method == 'function')
        return process.nextTick(invokeMethod,method,msg);

    if (Options.fallbackToSuds)
        return process.nextTick(tryWithSuds,msg);

    process.nextTick(
        broadcast,
        msg.channel,
        {
            code: Options.codes.execute.methodNotExists,
            etag: msg.properties.headers.etag
        }
    );
    return msg.ack(true);
}

function handleResponse (msg, err, result) {
    logTime(msg.body.url,msg.body.method,msg.requestStart);

    if (err) {
        if (Options.fallbackToSuds)
            return process.nextTick(tryWithSuds,msg);
        var code = Options.codes.execute.methodError;
        var result = err.message;
    } else {
        var code = Options.codes.execute.ok;
    }

    process.nextTick(
        broadcast,
        msg.channel,
        {
            code: code,
            etag: msg.properties.headers.etag
        },
        new Buffer(JSON.stringify({
            responseMsg: result
        })).toString('base64')
    );
    return msg.ack(true);
}

function invokeMethod (method, msg) {
    msg.requestStart = moment();
    method(
        msg.body.args || {},
        handleResponse.bind(null,msg),
        {
            timeout: msg.body.timeout || Options.invokeTimeout
        }
    );
}

function logTime (url, method, start) {
    console.log('Response from:',url + '/' + method,':: in',moment.utc(moment().diff(start)).format('HH:mm.ss:SSS'));
}

function run (msg) {
    try {
        msg.body = JSON.parse(new Buffer(msg.body.toString(),'base64').toString('utf8'));
    } catch (err) {
        if (err instanceof SyntaxError)
            return msg.ack(true);
        throw err;
    }
    return process.nextTick(validate,msg);
}

function tryWithSuds (msg) {
    Publish.push(
        msg.channel,
        Options.tornpackRabbitmqExchange.default,
        routingKey(Options.services.sudsExecute),
        new Buffer(JSON.stringify(msg.body)).toString('base64'),
        {
            headers: msg.properties.headers
        }
    );
    return msg.ack(true);
}

function validate (msg) {
    if (!msg.properties.headers.etag)
        return msg.ack(true);
    if (!msg.body.url)
        return msg.ack(true);
    if (!msg.body.method)
        return msg.ack(true);
    if (msg.body.args !== undefined && !(msg.body.args instanceof Object))
        return msg.ack(true);
    msg.wsdl_headers = {};
    return process.nextTick(auth,msg);
}

module.exports = run;
