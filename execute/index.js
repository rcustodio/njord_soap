const channelName = require('amqp-node/static').channelName;
const queueName = require('amqp-node/static').queueName;
const Options = require('../options');
const RabbitMQ = require('amqp-node');
const routingKey = require('amqp-node/static').routingKey;
const Task = require('./task');
const uuid4 = require('uuid4');

function run (name) {
    var _channel = null;
    var _engine = null;
    var _queue = queueName(name);
    var _uid = uuid4();

    function call (msg) {
        function ack (result) {
            if (result)
                return _channel.ack(msg);
            return _channel.nack(msg);
        }

        Task({
            body: msg.content,
            channel: _channel,
            properties: msg.properties,
            ack: ack
        });
    }

    function consumer () {
        _engine.consumer.consume(
            _channel,
            _queue,
            call
        );
    }

    function onEngine (engine) {
        _engine = engine;
        _engine.channel(
            channelName(Options.rabbitmqChannel,_uid),
            {
                prefetch: Options.rabbitmqPrefetch
            },
            function (channel) {
                _channel = channel;
                process.nextTick(queueDeclare);
            }
        );
    }

    function queueBind () {
        _engine.queue.bind(
            _channel,
            _queue,
            Options.tornpackRabbitmqExchange.default,
            routingKey(name),
            consumer
        );
    }

    function queueDeclare () {
        _engine.queue.declare(
            _channel,
            queueName(name),
            {
                durable: true
            },
            queueBind
        );
    }

    RabbitMQ.engine(
        Options.rabbitmqName,
        onEngine
    );
}

module.exports = run;
