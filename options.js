var args = require('args');
var config = require('./config');

var optionsValues = args.Options.parse([
    {
        name: 'help',
        shortName: 'h',
        type: 'bool',
        help: 'help'
    },
    {
        name: 'env',
        type: 'string',
        defaultValue: './dev.js',
        help: 'Config file'
    },
    {
        name: 'fallbackToSuds',
        type: 'bool',
        defaultValue: false,
        help: 'On fail send to suds service'
    },
    {
        name: 'invokeTimeout',
        type: 'int',
        defaultValue: config.invokeTimeout,
        help: 'Timeout to method response'
    },
    {
        name: 'rabbitmqPrefetch',
        type: 'int',
        defaultValue: config.rabbitmqPrefetch,
        help: 'RabbitMQ channels prefetch'
    },
    {
        name: 'rabbitmqUrl',
        type: 'string',
        defaultValue: config.rabbitmqUrl,
        help: 'RabbitMQ url'
    }
]);

var options = args.parser(process.argv).parse(optionsValues);
if (options.help) {
    console.log(optionsValues.getHelp());
    process.exit();
}

var env = require(options.env);

for (i in options)
    config[i] = options[i];

for (i in env)
    config[i] = env[i];

module.exports = config;
