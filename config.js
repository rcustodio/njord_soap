module.exports = {
    codes: {
        execute: {
            clientError: '7905aff0294f43cdbf941af06d080712',
            methodError: '690a193bfb724599b24fe855b3929708',
            methodNotExists: '65fbc13f0699438e8820110cdfa255cd',
            ok: '1ecd24c960f04003885ae39587657bfc'
        }
    },
    fallbackToSuds: false,
    invokeTimeout: 60000,
    rabbitmqChannel: 'njord',
    rabbitmqName: 'njord_soap',
    rabbitmqPrefetch: 10,
    rabbitmqUrl: 'amqp://guest:guest@localhost:5672/%2f',
    services: {
        execute: 'njord_soap_execute',
        sudsExecute: 'njord_soap_suds_execute'
    },
    tornpackRabbitmqExchange: {
        default:'tornpack',
        headers:'tornpack_headers',
        headersAny:'tornpack_headers_any'
    }
};
