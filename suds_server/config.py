from tornpack.options import define

define('njord_soap_suds',default={
    'codes':{
        'execute':{
            'client_error':'7905aff0294f43cdbf941af06d080712',
            'method_error':'690a193bfb724599b24fe855b3929708',
            'method_not_exists':'65fbc13f0699438e8820110cdfa255cd',
            'ok':'1ecd24c960f04003885ae39587657bfc'
        }
    },
    'services':{
        'execute':'njord_soap_suds_execute'
    }
})

define('invoke_timeout',default=60000)
